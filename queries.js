const Pool = require("pg").Pool;

const pool = new Pool({
  user: "me",
  host: "localhost",
  database: "api",
  password: "password",
  port: 5432,
});

const getUsers = (_, response) => {
  pool.query(
    `
        SELECT * FROM users 
        ORDER BY id ASC`,
    (error, results) => {
      if (error) {
        throw error;
      }

      response.status(200).json(results.rows);
    }
  );
};

const getUserById = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query(`SELECT * FROM users WHERE ID = ${id}`, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.row);
  });
};

const createUser = (request, response) => {
  const { name, email } = request.body;

  pool.query(
    `INSERT INTO users (name, email) VALUES ('${name}', '${email}')`,
    (error, _) => {
      if (error) {
        throw error;
      }

      response.status(201).send(`User added`);
    }
  );
};

const updateUser = (request, response) => {
  const id = parseInt(request.params.id);
  const { name, email } = request.body;

  const q = `UPDATE users SET name = '${name}', email = '${email}', WHERE ID = ${id}`;
  console.log(q);
  pool.query(
    `UPDATE users SET name = '${name}', email = '${email}' WHERE ID = ${id}`,
    (error, _) => {
      if (error) {
        throw error;
      }
      response.status(200).send(`User modified with ID: ${id}`);
    }
  );
};

const deleteUser = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query(`DELETE FROM users WHERE id = ${id}`, (error, _) => {
    if (error) {
      throw error;
    }
    response.status(200).send(`User delete with ID: ${id}`);
  });
};

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
};
